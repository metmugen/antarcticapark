﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AntarcticaPark1.Startup))]
namespace AntarcticaPark1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
